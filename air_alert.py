#!/home/azureuser/miniconda3/envs/air-alert/bin/python

"""
Alert if nearby Purple Air sensors show air quality getting meaningfully better or worse.
"""

import datetime as dt
import json
from pathlib import Path
import requests
import pandas as pd
from numpy import median
from loguru import logger
import notifiers


## Config
project_dir = Path("/home/azureuser/air_alert")
log_file = project_dir / "log.txt"
results_file = project_dir / "results.csv"

logger.add(
    log_file,
    rotation="100 MB", 
    format="<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{line}</cyan>: <level>{message}</level>"
)
logger.info("Starting new run.")

devices = [
    '18471',
    '19317',
    '20165',
    '22475',
    '32985',
    '38607',
    '63985',
]

data_url = f"https://www.purpleair.com/json?show={'|'.join(devices)}"
alert_threshold = 12  # micrograms per cubic meter (not EPA AQI)
stale_hours = 6


## Get sensor data and compute summary statistic.
logger.info(f"Fetching data for {len(devices)} devices.")

response = requests.get(data_url)
data = response.json()['results']
values = [json.loads(x['Stats'])['v1'] for x in data]

logger.info(f"Retrieved PM2.5 values from {len(values)} sensors.")

pm25 = median(values)
num_sensors = len(values)
now = dt.datetime.now()

## Load previous results.
logger.info("Loading previous results from file.")

df = pd.read_csv(results_file, parse_dates=['ts', 'ts_previous'])
pm25_previous = df.iloc[-1]['pm25']
ts_previous = df.iloc[-1]['ts']

## Decide whether to alert.
alert = "none"
delta_hours = (now - ts_previous).total_seconds() / (60 * 60)

if pm25 >= alert_threshold:
    if (delta_hours >= stale_hours) or (pm25_previous < alert_threshold):
        alert = "high"

else:
    if pm25_previous >= alert_threshold and delta_hours < stale_hours:
        alert = "low"

## Append new result to results file.
logger.info("Appending new result to results file.")

result = pd.DataFrame([{
    "ts": now,
    "num_sensors": num_sensors,
    "pm25": pm25,
    "ts_previous": ts_previous,
    "pm25_previous": pm25_previous,
    "alert": alert
}])

result.to_csv(results_file, mode="a", index=False, header=False)

## If applicable, send the alert.
if alert != "none":
    logger.info("Sending alert!")
    gmail = notifiers.get_notifier('gmail')
    gmail.notify(
        to="briankent@fastmail.com",
        subject=f"Air quality alert: PM2.5 is {alert}!",
        message=(
            f"Current PM2.5 is {pm25} micrograms/m^3.\n"
            f"Previous PM2.5 was {pm25_previous}, at {ts_previous:%Y-%m-%d %H:%M} UTC."
        )
    )

logger.info("Run finished successfully.")
